import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 */
public class SampleTest {


    @Test
    public void concurrentModificationTest() throws InterruptedException {

        Executor executor = Executors.newCachedThreadPool();

        EventItemFactory<EventItem<? extends GetValFromByte>> factory2= EventItemFactory.getInstance();
        int bufferSize=1024;
        Disruptor<EventItem<? extends GetValFromByte>> disruptor= new Disruptor<EventItem<? extends GetValFromByte>>(factory2,bufferSize,executor);
        disruptor.handleEventsWith(new EventItemHandler());
        disruptor.start();
        RingBuffer<EventItem<? extends GetValFromByte>> itemRingBuffer=disruptor.getRingBuffer();
        EventItemProducerWithTranslator producer= new EventItemProducerWithTranslator(itemRingBuffer);
        ByteBuffer bb= ByteBuffer.allocate(8);
        for(long i=0;true;i++){

            bb.putLong(0,i);
            producer.onData(bb);
            Thread.sleep(100);

        }



    }



}
