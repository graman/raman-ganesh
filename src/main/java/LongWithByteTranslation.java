/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import java.nio.ByteBuffer;

public class LongWithByteTranslation extends Number implements GetValFromByte<LongWithByteTranslation,ByteBuffer,EventItem> {

    private  Long value=0l;



    public LongWithByteTranslation getValueFromByteArray(ByteBuffer b) {
       LongWithByteTranslation item= new LongWithByteTranslation();
        item.setValue(b.getLong());
        return item;
    }

    @Override
    public int intValue() {
        return value.intValue();
    }

    @Override
    public long longValue() {
        return value;
    }

    @Override
    public float floatValue() {
        return value.floatValue();
    }

    @Override
    public double doubleValue() {
        return value.doubleValue();
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }




    @Override
    public void translateTo(EventItem event, long sequence, ByteBuffer arg0) {
        long aLong = arg0.getLong(0);
        LongWithByteTranslation longWithByteTranslation = new LongWithByteTranslation();
        longWithByteTranslation.setValue(aLong);
        event.setObject(longWithByteTranslation);
    }

    @Override
    public String toString() {
        return "LongWithByteTranslation{" +
                "value=" + value +
                '}';
    }
}
